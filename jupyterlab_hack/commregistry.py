# Singleton class, registers Comm for application objects

import logging
import os

# Set up logger "commregistry" at jupyterlab_hack/__logs__/commregisty.log
basename = os.path.basename(__file__)
dirname = os.path.abspath(os.path.dirname(__file__))

log_name, ext = os.path.splitext(basename)
log_filename = '{}.log'.format(log_name)
log_path = os.path.join(dirname, '__logs__', 'commregistry.log')

logger = logging.getLogger(log_name)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(log_path, 'w')
fh.setLevel(logging.DEBUG)
logger.addHandler(fh)


from ipykernel.comm import Comm
COMM_PREFIX = 'hack'
COMM_NAME = COMM_PREFIX + '_comm_target'
COMM_ID_NAME = COMM_PREFIX + '_comm_id'
REGISTRY_ID_NAME =  COMM_PREFIX + '_registry_id'


class CommRegistry:
    '''Singleton class for handling Comm I/O with front-end renderers

    Local objects call register_comm target in order to received messages
    from the renderers. Objects must have on_msg() method
    '''
    _instance = None

    def __init__(self):
        ''''''
        logger.info('Initialize CommRegistry')
        assert (self.__class__._instance is None), \
            'ERROR cannot instantiate multiple CommRegistry instances'
        self._comm = None            # Comm for custrom renderer I/O
        self._registry_count = 0     # for assigning registry ids
        self._registry_map = dict()  # maps instance id to object
        self._message_log = list()   # for test/debug

    @classmethod
    def get_instance(cls):
        ''''''
        if cls._instance is None:
            cls._instance = CommRegistry()
        return cls._instance

    def register_comm_target(self, comm_target):
        '''Registers object for comm I/O

        Returns dictionary of meta data that *must* be sent to renderer,
        so that Comm targets can receive messages from their renderers.
        '''
        if self._comm is None:
            self._comm = Comm(target_name=COMM_NAME)
            self._comm.on_msg(self.__class__._receive_message)

        self._registry_count += 1
        registry_id = self._registry_count
        self._registry_map[registry_id] = comm_target

        return {
            COMM_ID_NAME: self._comm.comm_id,
            REGISTRY_ID_NAME: registry_id
        }

    def _lookup_comm_target(self, registry_id):
        return self._registry_map.get(registry_id)

    @staticmethod
    def _receive_message(msg):
        '''Handles messages received from the renderer

        '''
        CommRegistry.get_instance()._message_log.append(msg)
        logger.info('Received message {}'.format(msg))

        registry_id = msg.get('metadata', {}).get(REGISTRY_ID_NAME)
        if registry_id is None:
            logger.error('ERROR Input message missing {}'.format(REGISTRY_ID_NAME))
            return

        registry = CommRegistry.get_instance()
        comm_target = registry._lookup_comm_target(registry_id)
        if comm_target is None:
            logger.error('ERROR Unrecognized registry_id {}'.format(registry_id))
            return

        if not callable(comm_target.on_msg):
            logger.error('ERROR Comm target {} missing on_msg() method'.format(comm_target))
            return

        content = msg.get('content')
        if content is None:
            logger.error('ERROR Incoming message missing \"content\"')
            return

        data = content.get('data')
        if content is None:
            logger.error('ERROR Incoming message.content missing \"data\"')
            return

        value = data.get('value')
        if content is None:
            logger.error('ERROR Incoming message.content.data missing \"value\"')
            return

        # logger.debug('Calling on_msg with value \"{}\"'.format(value))
        # fh.flush()
        comm_target.on_msg(value)

    def _send_message(self, data):
        '''Convenience method, primarily for testing

        '''
        if self._comm is None:
            self._comm = Comm(target_name=COMM_NAME)
            self._comm.on_msg(self.__class__._receive_message)
        self._comm.send(data)
