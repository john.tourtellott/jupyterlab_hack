from IPython.display import display, JSON
from .commregistry import CommRegistry

MIME_TYPE = 'application/hack+json'

class Test(JSON):
    def __init__(self):
        self.value = {
            'inputText': '(null)'
        }
        # register as a Comm target
        self.metadata = CommRegistry.get_instance().register_comm_target(self)

    def on_msg(self, msg):
        self.value = msg
        print('Received message {}'.format(msg))

    def _ipython_display_(self):
        bundle = {}
        bundle[MIME_TYPE] = {
            'data': {'value': self.value}
        }

        display(bundle, metadata=self.metadata, raw=True)
