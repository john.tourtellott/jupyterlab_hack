# jupyterlab_hack

Trying to homebrew an extension, mostly by copying from the geojson example at
https://github.com/jupyterlab/jupyter-renderers/tree/master/packages/geojson-extension

Use build instructions for that example. FYI "The jlpm command is JupyterLab's pinned version of yarn that is installed with JupyterLab. You may use yarn or npm in lieu of jlpm below."


```bash
# Install dependencies - use npm to avoid package-version conflicts with jlpm
npm install
# Build Typescript source
jlpm build
# Link your development version of the extension with JupyterLab
jupyter labextension link .


# After making changes:
# Rebuild Typescript source
jlpm build
# Rebuild JupyterLab
jupyter lab build
```
