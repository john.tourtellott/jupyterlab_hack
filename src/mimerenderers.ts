/**
 * Create rendermime plugins for rendermime extension modules.
 * This code copied/adapted from @jupyterlab/application/lib/mimerenderers.js
 */

import { JupyterLabPlugin, ILayoutRestorer } from '@jupyterlab/application';
import { InstanceTracker } from '@jupyterlab/apputils';
import { MimeDocumentFactory } from '@jupyterlab/docregistry';
import { IRenderMimeRegistry } from '@jupyterlab/rendermime';
import { IRenderMime } from '@jupyterlab/rendermime-interfaces';

export
function createRendermimePlugin(item: IRenderMime.IExtension): JupyterLabPlugin<void> {
    return {
        id: item.id,
        requires: [ILayoutRestorer, IRenderMimeRegistry],
        autoStart: true,
        activate: function (app, restorer: ILayoutRestorer, rendermime: IRenderMimeRegistry) {
            console.log('activating IRenderMime plugin');
            // Add the mime renderer.
            if (item.rank !== undefined) {
                rendermime.addFactory(item.rendererFactory, item.rank);
            }
            else {
                rendermime.addFactory(item.rendererFactory);
            }
            // Handle the widget factory.
            if (!item.documentWidgetFactoryOptions) {
                return;
            }
            var registry = app.docRegistry;
            var options = [];
            if (Array.isArray(item.documentWidgetFactoryOptions)) {
                options = item.documentWidgetFactoryOptions;
            }
            else {
                options = [item.documentWidgetFactoryOptions];
            }
            if (item.fileTypes) {
                item.fileTypes.forEach(function (ft: any) {
                    app.docRegistry.addFileType(ft);
                });
            }
            options.forEach(function (option: any) {
                var factory = new MimeDocumentFactory({
                    renderTimeout: item.renderTimeout,
                    dataType: item.dataType,
                    rendermime: rendermime,
                    modelName: option.modelName,
                    name: option.name,
                    primaryFileType: registry.getFileType(option.primaryFileType),
                    fileTypes: option.fileTypes,
                    defaultFor: option.defaultFor
                });
                registry.addWidgetFactory(factory);
                var factoryName = factory.name;
                var namespace = factoryName + "-renderer";
                var tracker = new InstanceTracker({ namespace: namespace });
                // Handle state restoration.
                restorer.restore(tracker, {
                    command: 'docmanager:open',
                    args: function (widget: any) { return ({ path: widget.context.path, factory: factoryName }); },
                    name: function (widget: any) { return widget.context.path; }
                });
                factory.widgetCreated.connect(function (sender: any, widget: any) {
                    // Notify the instance tracker if restore data needs to update.
                    widget.context.pathChanged.connect(function () { tracker.save(widget); });
                    tracker.add(widget);
                });
            });
        }
    };
}
