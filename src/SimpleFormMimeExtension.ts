/**
 * A mime renderer factory for data.
 */

import { IRenderMime } from '@jupyterlab/rendermime-interfaces';

import { MIME_TYPE, SimpleFormWidget } from './SimpleFormWidget';

export const rendererFactory: IRenderMime.IRendererFactory = {
  safe: true,
  mimeTypes: [MIME_TYPE],
  createRenderer: options => new SimpleFormWidget(options)
};


export
const SimpleFormMimeExtension: IRenderMime.IExtension  = {
    id: 'jupyterlab_hack:factory',
    rendererFactory,
    rank: 0,
    dataType: 'json',
    fileTypes: [
      {
        name: 'hack',
        mimeTypes: [MIME_TYPE],
        extensions: ['.hack'],
      }
    ],
    documentWidgetFactoryOptions: {
      name: 'Hack',
      primaryFileType: 'hack',
      fileTypes: ['hack', 'json'],
      defaultFor: ['hack']
    }
  };
