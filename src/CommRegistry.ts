import { Kernel, KernelMessage } from '@jupyterlab/services';

export
class SingletonCommRegistry {
  constructor() {
    this._comms = {};
  }

  registerComm(comm: Kernel.IComm, msg: KernelMessage.IMessage): void {
    console.log(`Open comm ${comm.commId}`);

    this._comms[comm.commId] = comm;
    // console.log('this._comms:');
    // console.dir(this._comms);
    comm.onClose = this.handleClose;
    comm.onMsg = this.handleMessage;
  }

  commInstance(commId: string) {
    // console.log(`Checking for commId ${commId} in this._comms:`);
    // console.dir(this._comms);
    if (commId in this._comms) {
      return this._comms[commId];
    }
    // (else)
    return null;
  }

  handleClose(msg: KernelMessage.ICommCloseMsg) {
    console.log('Received close message:');
    console.dir(msg);
    let commId = msg.content.comm_id;
    delete this._comms[commId];
  }

  handleMessage(msg: KernelMessage.IMessage) {
    console.log('Received message:');
    console.dir(msg);
  }

  // Table of   // <comm_id, comm instance>
  _comms: { [id: string]: Kernel.IComm } = {};

}
let CommRegistry = new SingletonCommRegistry();
export default CommRegistry;
