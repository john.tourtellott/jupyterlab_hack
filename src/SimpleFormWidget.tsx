import { IRenderMime } from '@jupyterlab/rendermime-interfaces';
import { Kernel } from '@jupyterlab/services';

import { Widget } from '@phosphor/widgets';

import * as React from 'react';
import * as ReactDOM from 'react-dom';

import '../style/index.css';

/**
 * The CSS class to add to the Widget.
 */
const CSS_CLASS = 'jp-JupyterLabHack';

/**
 * The MIME type.
 */
export const MIME_TYPE = 'application/hack+json';

import SimpleFormComponent from './SimpleFormComponent';
import CommRegistry from './CommRegistry';


export class SimpleFormWidget extends Widget implements IRenderMime.IRenderer {
  /**
   * Create a new widget for rendering.
   */
  constructor(options: IRenderMime.IRendererOptions) {
    super();
    this.addClass(CSS_CLASS);
    this._mimeType = options.mimeType;
    console.log('SimpleFormWidget constructor');
  }

  /**
   * Dispose of the widget.
   */
  dispose(): void {
    super.dispose();
  }

  /**
   * Render into this widget's node.
   */
  renderModel(model: IRenderMime.IMimeModel): Promise<void> {
    console.dir(model);
    const data = model.data[this._mimeType] as any | {};
    //console.dir(data);
    //this.node.textContent = 'Hello from the SimpleFormWidget'

    // this.node.innerHTML = `
    // <p>Hello from SimpleFormWidget</p>
    // <p>
    //   <input type="text" id="theText" placeholder="Enter text to send" />
    //   <button onclick="sendText()">Send To Kernel</button>
    // </p>
    // `;

    const metadata = model.metadata as any || {};
    // console.log('metadata:');
    // console.dir(metadata);
    const commId: string = metadata.hack_comm_id || '';
    const comm: Kernel.IComm = CommRegistry.commInstance(commId);
    const props = { data, metadata, comm };
    // console.log('props to SimpleFormComponent:');
    // console.dir(props);

    ReactDOM.render(<SimpleFormComponent {...props} />, this.node);
    return Promise.resolve();
  }

  sendText(): void {
    console.log('SimpleFormWidget sendText()');
  }

  private _mimeType: string;
}
