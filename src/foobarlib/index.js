// Small javascript module to test inserting into extension

class Foo {
  constructor() {
    let dt = new Date();
    console.log(`Foo constructor at ${dt}`);
  }

  getMessage() {
    return 'Hello from da Foo';
  }
}

class Bar {
  constructor() {
    let dt = new Date();
    console.log(`Bar constructor at ${dt}`);
  }
}

export { Foo, Bar };
