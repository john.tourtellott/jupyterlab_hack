var path = require('path');
var webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');

//const sourcePath = path.join(__dirname, './src');

module.exports = {
  entry: path.join(__dirname, './src/foobarlib/index.js'),
  output: {
    path: path.join(__dirname, 'lib'),
    filename: 'JUPYTERLAB_FILE_LOADER_foobarlib.js',
    // Using commonjs2 as library target, so that node scripts can also load it
    // for standalone testing.
    // In production, should probably use umd (until webpack adds support for es6)
    libraryTarget: 'commonjs2',
  },
  mode: 'development',
  devtool: 'source-map',
  // resolve: {
  //   modules: [
  //     path.resolve(__dirname, 'node_modules'),
  //     sourcePath
  //   ]
  // },
  //target: 'node',
  plugins: [
    // Ignore require() calls in vs/language/typescript/lib/typescriptServices.js
    new webpack.IgnorePlugin(
      /^((fs)|(path)|(os)|(crypto)|(source-map-support))$/,
      /vs\/language\/typescript\/lib/
    ),
    new CopyWebpackPlugin([{
      from: './src/foobarlib/index.d.ts',
      to: 'JUPYTERLAB_FILE_LOADER_foobarlib.d.ts',
      toType: 'file'
    }])
  ]
};
